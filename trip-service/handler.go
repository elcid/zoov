package main

import (
	"log"

	bikeProto "gitlab.com/Monito_b/zoov/bike-service/proto/bike"
	pb "gitlab.com/Monito_b/zoov/trip-service/proto/trip"
	mgo "gopkg.in/mgo.v2"

	"golang.org/x/net/context"
)

type service struct {
	session    *mgo.Session
	bikeClient bikeProto.BikeServiceClient
}

func (s *service) GetRepo() Repository {
	return &TripRepository{s.session.Clone()}
}

func (s *service) StartTrip(ctx context.Context, req *pb.Bike, res *pb.Response) error {

	// Here we call a client instance of our bike service with our id,
	bikeResponse, err := s.bikeClient.GetBike(context.Background(), &bikeProto.Specification{Id: req.Id})

	log.Printf("Found bike: %s  and status %d\n", bikeResponse.Bike.Id, bikeResponse.Bike.Status)

	if err != nil {
		return err
	}

	if bikeResponse.Bike.Status == 1 {
		res.Created = false
		return nil
	}

	tripid, err := s.GetRepo().Start(bikeResponse.Bike)

	if err != nil {
		return err
	}

	log.Printf("Trip id returned: %v", tripid)
	res.Created = true
	res.Tripid = tripid
	return nil
}

func (s *service) EndTripByBike(ctx context.Context, req *pb.Bike, res *pb.Response) error {

	updated, err := s.bikeClient.UpdateStatusBike(context.Background(), &bikeProto.Specification{Id: req.Id, Status: 0})
	trip, err := s.GetRepo().UpdateTrip(req)

	if err != nil || updated.Available == false {
		return err
	}
	log.Printf("Trip id %v", trip)
	res.Trip = trip
	return err
}
