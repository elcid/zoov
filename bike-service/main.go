package main

import (

	"fmt"
	"log"

	pb "gitlab.com/Monito_b/zoov/bike-service/proto/bike"
	micro "github.com/micro/go-micro"
	"gopkg.in/mgo.v2"


	"os"
)

const (
	defaultHost = "localhost:27017"
	IsDrop = true
)

func main() {

	host := os.Getenv("DB_HOST")

	if host == "" {
		host = defaultHost
	}

	session, err := CreateSession(host)

	// Mgo creates a 'master' session, we need to end that session
	// before the main function closes.
	defer session.Close()

	if err != nil {
		log.Panicf("Could not connect to datastore with host %s - %v", host, err)
	}

	// Drop Database
	if IsDrop {
		err = session.DB(dbName).DropDatabase()
		if err != nil {
			panic(err)
		}
	}

	// Collection Bike
	c := session.DB(dbName).C(bikeCollection)

	// Index
	index := mgo.Index{
		Key:        []string{"id"},
		Unique:     true,
		DropDups:   true,
		Background: true,
		Sparse:     true,
	}

	err = c.EnsureIndex(index)

	if err != nil {
		log.Panicf("Could not create index in datastore with host %s - %v", host, err)

	}


	// Create a new service. Optionally include some options here.
	srv := micro.NewService(

		// This name must match the package name given in your protobuf definition
		micro.Name("go.micro.srv.bike"),
		micro.Version("latest"),
	)

	// Init will parse the command line flags.
	srv.Init()

	// Register handler
	pb.RegisterBikeServiceHandler(srv.Server(), &service{session})

	// Run the server
	if err := srv.Run(); err != nil {
		fmt.Println(err)
	}
}
