# Zoov - Test Microservices in Go with Docker and MongoDB

Overview
========

This service will execute the bike-service and trip-service to demostrate how il could be execute from a gateway if necesary

it allows:
	load the bikes test
	load the trip test
	call the services capabilities
    


Requirements
===========

* Docker 1.12
* Docker Compose 1.8

Build services
==============================

```
Make build
```


Starting services
==============================

```
make run
```

Stoping services
==============================

```
Docker <id> stop
```

Note: If you want to comunicate with the other services it should be running in dockerland with docker-compose.

	But in standalone should be running mongodb in port default

```
docker-compose build zoov_client
```

