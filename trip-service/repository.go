package main

import (
	"time"
	"log"

	pb "gitlab.com/Monito_b/zoov/trip-service/proto/trip"
	bikeProto "gitlab.com/Monito_b/zoov/bike-service/proto/bike"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)


const (
	dbName = "zoov-db"
	tripCollection = "trips"
)

type Repository interface {
	Start(bike *bikeProto.Bike) (string, error)
	UpdateTrip(bike *pb.Bike) (*pb.Trip, error)
	Close()
}

type TripRepository struct {
	session *mgo.Session
}

// Create a new bike
func (repo *TripRepository) Start(bike *bikeProto.Bike) (string, error) {

	// var location *pb.Location
	// var coordinates *pb.Point

	log.Println("bike Id: ", bike.Id, "lat", bike.Location.Coordinates.Latitude)
	// location.Type = "Point"
	// coordinates.Latitude = bike.Location.Coordinates.Latitude
	// coordinates.Longitude = bike.Location.Coordinates.Longitude


	st := time.Now()
	obj_id := bson.NewObjectId()
	locations := new(pb.Location)
	arlocations := []*pb.Location{}
	arlocations = append(arlocations, locations)
	// point := &pb.Point{Latitude: bike.Location.Coordinates.Latitude, Longitude: bike.Location.Coordinates.Longitude}
	trip := &pb.Trip{Id: obj_id.String(), Status: 1, BikeId: bike.Id, Locations: arlocations, StartedAt: st.Format(time.RFC3339)}
	log.Println(st, obj_id.String(), trip)
	// trip.Status = 1
	// trip.BikeId = bike.Id
	// trip.Locations = append(trip.Locations, location)
	// trip.StartedAt = st.Format(time.RFC3339)
	// trip.EndedAt = ""

	if err := repo.collection().Insert(trip); err != nil {
		return "", err
	}

	if err := repo.collection().Find(bson.M{"bike_id": bike.Id}).One(&trip); err != nil {
		return "", err
	}
	return trip.Id, nil
}

func (repo *TripRepository) UpdateTrip(bike *pb.Bike) (*pb.Trip, error) {

	var trip *pb.Trip

	et := time.Now()
	bikeToUpdate := bson.M{"bike_id": bike.Id}
	tripChange := bson.M{"$set": bson.M{"ended_at": et.Format(time.RFC3339), "status": 0}}
	if err := repo.collection().Update(bikeToUpdate, tripChange); err != nil {
		return nil, err
	}

	if err := repo.collection().Find(bikeToUpdate).One(&trip); err != nil {
		return nil, err
	}

	return trip, nil
}


func (repo *TripRepository) Close() {
	repo.session.Close()
}

func (repo *TripRepository) collection() *mgo.Collection {

	return repo.session.DB(dbName).C(tripCollection)
}

