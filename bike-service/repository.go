package main

import (
	pb "gitlab.com/Monito_b/zoov/bike-service/proto/bike"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

const (
	dbName = "zoov-db"
	bikeCollection = "bikes"
)

type Repository interface {
	Create(bike *pb.Bike) error
	GetAll() ([]*pb.Bike, error)
	Get(spec *pb.Specification) (*pb.Bike, error)
	Update(spec *pb.Specification) (bool, error)
	Close()
}

type BikeRepository struct {
	session *mgo.Session
}

// Create a new bike
func (repo *BikeRepository) Create(bike *pb.Bike) error {
	return repo.collection().Insert(bike)
}

// GetAll bikes
func (repo *BikeRepository) GetAll() ([]*pb.Bike, error) {
	var bikes []*pb.Bike
	// Find normally takes a query, but as we want everything, we can nil this.
	// We then bind our bikes variable by passing it as an argument to .All().
	// That sets bikes to the result of the find query.
	// There's also a `One()` function for single results.
	err := repo.collection().Find(nil).All(&bikes)
	return bikes, err
}

func (repo *BikeRepository) Get(spec *pb.Specification) (*pb.Bike, error) {
	var bike *pb.Bike

	if err := repo.collection().Find(bson.M{"id": spec.Id}).One(&bike); err != nil {
		return nil, err
	}
	return bike, nil
}

func (repo *BikeRepository) Update(spec *pb.Specification) (bool, error) {
	bikeToUpdate := bson.M{"id": spec.Id}
	StatusChange := bson.M{"$set": bson.M{"status": spec.Status}}

	if err := repo.collection().Update(bikeToUpdate, StatusChange); err != nil {
		return false, err
	}

	return true, nil
}

func (repo *BikeRepository) Close() {
	repo.session.Close()
}

func (repo *BikeRepository) collection() *mgo.Collection {

	return repo.session.DB(dbName).C(bikeCollection)
}
